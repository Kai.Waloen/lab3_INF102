package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    private int peak;

    @Override
    public int peakElement(List<Integer> numbers) {
        // Implement me :)

        if (numbers.isEmpty()) {
            return 0;
        } else if (numbers.size() >= 3) {
            int firstNumber = numbers.get(0);
            int secondNumber = numbers.get(1);
            int thirdNumber = numbers.get(2);
            this.peak = 0;

            if (firstNumber >= secondNumber) {
                return firstNumber;
            } else if (firstNumber <= secondNumber && secondNumber >= thirdNumber) {
                return secondNumber;
            } else {
                numbers.remove(1);
                this.peak = peakElement(numbers);
            }
        } else if (numbers.size() == 2) {
            int firstNumber = numbers.get(0);
            int secondNumber = numbers.get(1);

            if (firstNumber >= secondNumber) {
                return firstNumber;
            } else if (firstNumber < secondNumber){
                return secondNumber;
            } else {
                this.peak = peakElement(numbers);
            }
        } else {
            return numbers.get(0);
        }

        return this.peak;
    }





}
