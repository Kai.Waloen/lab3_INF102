package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        // Implement me :)

        // got some help from ChatGPT to fix the recursion method
        // the method I came up with was mostly right and it ended up with the correct
        // total sum but it included an instance variable to add up the sum which was not
        // being carried over to the end of the method.


        if (list.size() != 0) {
            long firstNumber = list.get(0);
            list.remove(0);

            long recursion = sum(list);
            return firstNumber + recursion;
        } else {
            return 0;
        }
    }

}
