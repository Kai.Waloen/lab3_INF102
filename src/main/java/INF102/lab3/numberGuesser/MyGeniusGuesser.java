package INF102.lab3.numberGuesser;


import java.util.Random;

public class MyGeniusGuesser implements IGuesser {


    @Override
    public int findNumber(RandomNumber number) {
        return largerOrSmaller(number, number.getUpperbound(), number.getLowerbound());
        }

    private int findTheMidPoint(int upperBound, int lowerBound) {
        return (upperBound - lowerBound)/2 + lowerBound;
    }

    private int largerOrSmaller(RandomNumber number, int upper, int lower) {
        int mid = findTheMidPoint(upper, lower);

        if (number.guess(mid) == 0) {
            return mid;
        } else if (number.guess(mid) ==  -1) { //guess was too low
            return largerOrSmaller(number, upper, mid);
        } else {
            return largerOrSmaller(number, mid, lower);

        }

    }

}